#include "profiler.hpp"
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <boost/lexical_cast.hpp> 

using namespace std;

#define BILLION  1000000000L
#define MICROCONVERT 1000000.0

void profiler::startClockMeasure(){
	if (clock_gettime(CLOCK_MONOTONIC, &startClock) == -1){
		perror("clock gettime");
		exit(EXIT_FAILURE);
	}
}

void profiler::stopClockMeasure(){
	if (clock_gettime(CLOCK_MONOTONIC, &stopClock) == -1){
		perror("clock gettime");
		exit(EXIT_FAILURE);
	}
	diffClock = ((double)(stopClock.tv_sec - startClock.tv_sec) 
							+ (double)(stopClock.tv_nsec - startClock.tv_nsec) / BILLION) 
							* MICROCONVERT;	
}

profiler profiler::doStats(profiler p){
	if (clockMeasurements.size() == 0 || cycleMeasurements.size() == 0) exit(EXIT_FAILURE);

	p.clock_min = *std::min_element(clockMeasurements.begin(), clockMeasurements.end());
	p.clock_max = *std::max_element(clockMeasurements.begin(), clockMeasurements.end());

	p.cycle_min = *std::min_element(cycleMeasurements.begin(), cycleMeasurements.end());
	p.cycle_max = *std::max_element(cycleMeasurements.begin(), cycleMeasurements.end());
	//printf("Min, max: %f %f\n", p.min, p.max);

	double sum_clock = 0;
	double sum_cycle = 0;

	for(unsigned int j = 0; j < clockMeasurements.size(); j++){
		sum_clock += (double)clockMeasurements[j];
		sum_cycle += (double)cycleMeasurements[j];
	}

	p.clock_mean = sum_clock / (double)clockMeasurements.size();
	p.cycle_mean = sum_cycle / (double)cycleMeasurements.size();
	//printf("\nMean: %f \n", p.mean);

	double tmp1 = 0, tmp2 = 0;
	for(unsigned int i = 0; i < clockMeasurements.size(); i++){
		tmp1 += pow((double)clockMeasurements[i] - p.clock_mean, 2);
		tmp2 += pow((double)cycleMeasurements[i] - p.cycle_mean, 2);
	}
	p.clock_sd = sqrt((1.0 / (double)clockMeasurements.size()) * tmp1);
	p.cycle_sd = sqrt((1.0 / (double)cycleMeasurements.size()) * tmp2);
	//printf("SD: %f\n", p.sd);

	return p;
}

void profiler::writeToFile(profiler p, boost::multiprecision::cpp_int n, string fileName, int unit){
	FILE *cFile;

	if (unit == 1){
		if (clockMeasurements.size() == 0) exit(EXIT_FAILURE);

		if (FILE *cFile = fopen(fileName.c_str(), "r")){
			fclose(cFile);	
		}else{
			cFile = fopen(fileName.c_str(), "w");

			fprintf(cFile, "%5s\t%10s\t%10s\t%12s\t%10s\t%20s\n", "# n", "min [us]", "max [us]", "mean [us]", 
							"sd [us]", "measurements [us]");
			fclose(cFile);
		}

		cFile = fopen(fileName.c_str(), "a");

		fprintf(cFile, "%s\t\t%f\t%f\t%f\t%f\t", boost::lexical_cast<std::string>(n).c_str(), p.clock_min, p.clock_max, p.clock_mean, p.clock_sd);

		for (vector<double>::const_iterator i = clockMeasurements.begin(); i != clockMeasurements.end(); ++i){
			fprintf(cFile, "%f ", *i);
		}
		fprintf(cFile, "\n");
		fclose(cFile);
		
	}

	if (unit == 2){
		if (cycleMeasurements.size() == 0) exit(EXIT_FAILURE);
		
		if (FILE *cFile = fopen(fileName.c_str(), "r")){
			fclose(cFile);	
		}else{
			cFile = fopen(fileName.c_str(), "w");
			fprintf(cFile, "%5s\t\t%10s\t%10s\t%12s\t%10s\t%20s\n", "# n", "min [cycles]", "max [cycles]", 
							"mean [cycles]", "sd [cycles]", "measurements [cycles]");
			fclose(cFile);
		}
		
		cFile = fopen(fileName.c_str(), "a");

		//printf("%s\n", boost::lexical_cast<std::string>(n).c_str());

		fprintf(cFile, "%s\t%f\t%f\t%f\t%f\t", boost::lexical_cast<std::string>(n).c_str(), p.cycle_min, p.cycle_max, p.cycle_mean, p.cycle_sd);

		for (vector<double>::const_iterator i = cycleMeasurements.begin(); i != cycleMeasurements.end(); ++i){
			fprintf(cFile, "%f ", *i);
		}

		fprintf(cFile, "\n");
		fclose(cFile);
			
	}
	
}
