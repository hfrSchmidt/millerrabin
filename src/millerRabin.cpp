
#include "millerRabin.hpp"


#ifdef MEASURE 
#include "profiler.hpp"

// Number of measurements used for statistics
#define MEASUREMENTS_REP 5

// Maximum number to be tested for primality
#define MAX_NUM 10000000
#endif

#ifdef PLOTS
#include "profilerPlots.hpp"
#endif

using namespace std;
using namespace boost::multiprecision;

// passing -D{NAME} to g++ defines {NAME} with value 1

#ifndef ENABLE_TESTS 
int main(){

	#if defined(MEASURE) || defined(PLOTS) 

	string tmp_listfc[] = {"clockMeasurements_mr1.dat", "clockMeasurements_mr2.dat", \
												"clockMeasurements_mr3.dat","clockMeasurements_mr4.dat", \
												"clockMeasurements_mr5.dat","clockMeasurements_BoostMR.dat", \
												"clockMeasurements_mr5mpz.dat"};

	vector<string> filenamesClock(tmp_listfc, tmp_listfc + 7); 

	string tmp_listfcy[] = {"cycleMeasurements_mr1.dat", "cycleMeasurements_mr2.dat", \
												"cycleMeasurements_mr3.dat","cycleMeasurements_mr4.dat", \
												"cycleMeasurements_mr5.dat","cycleMeasurements_BoostMR.dat", \
												"cycleMeasurements_mr5mpz.dat"};

	vector<string> filenamesCycle(tmp_listfcy, tmp_listfcy + 7);

	#endif

	#ifdef MEASURE

	profiler pMR1, pMR2, pMR3, pMR4, pMR5, pMR6, pMR5mpz;

	for(cpp_int n = 5; n < MAX_NUM; n+=2){
		//cout << n << "  " << millerRabinBitShift2<cpp_int>(n) << endl;
		if (!(miller_rabin_test(n, MR_REPETITIONS))) continue;

		cout << n << endl;

		for (cpp_int i = 0; i < MEASUREMENTS_REP; i++){
			
			if (n < 4000){
				pMR1.startClockMeasure();
				pMR1.startCycle = pMR1.cycleMeasure();

				millerRabinQuadratic(n);
		
				pMR1.stopCycle = pMR1.cycleMeasure();
				pMR1.stopClockMeasure();
				pMR1.clockMeasurements.push_back(pMR1.diffClock);
				pMR1.cycleMeasurements.push_back(pMR1.stopCycle - pMR1.startCycle);		
			}
			
			if (n < 7000){
				pMR2.startClockMeasure();
				pMR2.startCycle = pMR2.cycleMeasure();

				millerRabinExpBySquaring(n);
		
				pMR2.stopCycle = pMR2.cycleMeasure();
				pMR2.stopClockMeasure();
				pMR2.clockMeasurements.push_back(pMR2.diffClock);
				pMR2.cycleMeasurements.push_back(pMR2.stopCycle - pMR2.startCycle);	
			}

			pMR3.startClockMeasure();
			pMR3.startCycle = pMR3.cycleMeasure();

			millerRabinFastModExp(n);
	
			pMR3.stopCycle = pMR3.cycleMeasure();
			pMR3.stopClockMeasure();
			pMR3.clockMeasurements.push_back(pMR3.diffClock);
			pMR3.cycleMeasurements.push_back(pMR3.stopCycle - pMR3.startCycle);

			pMR4.startClockMeasure();
			pMR4.startCycle = pMR4.cycleMeasure();

			millerRabinBitShift(n);
	
			pMR4.stopCycle = pMR4.cycleMeasure();
			pMR4.stopClockMeasure();
			pMR4.clockMeasurements.push_back(pMR4.diffClock);
			pMR4.cycleMeasurements.push_back(pMR4.stopCycle - pMR4.startCycle);

			pMR5.startClockMeasure();
			pMR5.startCycle = pMR5.cycleMeasure();

			millerRabinBitShift2(n);			
	
			pMR5.stopCycle = pMR5.cycleMeasure();
			pMR5.stopClockMeasure();
			pMR5.clockMeasurements.push_back(pMR5.diffClock);
			pMR5.cycleMeasurements.push_back(pMR5.stopCycle - pMR5.startCycle);

			pMR6.startClockMeasure();
			pMR6.startCycle = pMR6.cycleMeasure();

			miller_rabin_test(n, MR_REPETITIONS);
	
			pMR6.stopCycle = pMR6.cycleMeasure();
			pMR6.stopClockMeasure();
			pMR6.clockMeasurements.push_back(pMR6.diffClock);
			pMR6.cycleMeasurements.push_back(pMR6.stopCycle - pMR6.startCycle);

			mpz_int n_mpz = n.convert_to<mpz_int>();

			pMR5mpz.startClockMeasure();
			pMR5mpz.startCycle = pMR5mpz.cycleMeasure();

			millerRabinBitShift2(n_mpz);
	
			pMR5mpz.stopCycle = pMR5mpz.cycleMeasure();
			pMR5mpz.stopClockMeasure();
			pMR5mpz.clockMeasurements.push_back(pMR5mpz.diffClock);
			pMR5mpz.cycleMeasurements.push_back(pMR5mpz.stopCycle - pMR5mpz.startCycle);
		}
		
		if (n < 4000){
			pMR1 = pMR1.doStats(pMR1);
			pMR1.writeToFile(pMR1, n, filenamesClock[0], 1);
			pMR1.writeToFile(pMR1, n, filenamesCycle[0], 2);
			pMR1.clockMeasurements.clear();
			pMR1.cycleMeasurements.clear();		
		}

		if (n < 7000){
			pMR2 = pMR2.doStats(pMR2);
			pMR2.writeToFile(pMR2, n, filenamesClock[1], 1);
			pMR2.writeToFile(pMR2, n, filenamesCycle[1], 2);
			pMR2.clockMeasurements.clear();
			pMR2.cycleMeasurements.clear();	
		}
		
		pMR3 = pMR3.doStats(pMR3);
		pMR3.writeToFile(pMR3, n, filenamesClock[2], 1);
		pMR3.writeToFile(pMR3, n, filenamesCycle[2], 2);
		pMR3.clockMeasurements.clear();
		pMR3.cycleMeasurements.clear();

		pMR4 = pMR4.doStats(pMR4);
		pMR4.writeToFile(pMR4, n, filenamesClock[3], 1);
		pMR4.writeToFile(pMR4, n, filenamesCycle[3], 2);
		pMR4.clockMeasurements.clear();
		pMR4.cycleMeasurements.clear();

		pMR5 = pMR5.doStats(pMR5);
		pMR5.writeToFile(pMR5, n, filenamesClock[4], 1);
		pMR5.writeToFile(pMR5, n, filenamesCycle[4], 2);
		pMR5.clockMeasurements.clear();
		pMR5.cycleMeasurements.clear();

		pMR6 = pMR6.doStats(pMR6);
		pMR6.writeToFile(pMR6, n, filenamesClock[5], 1);
		pMR6.writeToFile(pMR6, n, filenamesCycle[5], 2);
		pMR6.clockMeasurements.clear();
		pMR6.cycleMeasurements.clear();

		pMR5mpz = pMR5mpz.doStats(pMR5mpz);
		pMR5mpz.writeToFile(pMR5mpz, n, filenamesClock[6], 1);
		pMR5mpz.writeToFile(pMR5mpz, n, filenamesCycle[6], 2);
		pMR5mpz.clockMeasurements.clear();
		pMR5mpz.cycleMeasurements.clear();
	}

	#endif

	#ifdef PLOTS
	makePlots(filenamesClock, filenamesCycle, 0);
	#endif

	#if !defined(MEASURE) && !defined(PLOTS) 

	return 0;

	#endif
	return 0;
}
#endif


// complexity: O(log n) assuming constant time for multiplication
template <typename T>
T expBySquaring(T base, T exponent){
	if (exponent == 0 ) return 1;
	if (exponent == 1 ) return base;

	T sq = base * base;

	if (exponent % 2 == 0) return expBySquaring<T>(sq, exponent/2);
	return base * expBySquaring<T>(sq, (exponent - 1)/2);
}
template cpp_int expBySquaring(cpp_int base, cpp_int exponent);
template mpz_int expBySquaring(mpz_int base, mpz_int exponent);

//calculates base^exponent mod m
// complexity: O(logN)
template <typename T>
T fastModularExp(T base, T exponent, T m){
	T s = base % m;
	T c = 1;

	while (exponent >= 1){
		if (exponent & 1) c = (c * s) % m;
		s = (s * s) % m;
		exponent /= 2;
	}
	return c;
}
template mpz_int fastModularExp(mpz_int a, mpz_int n, mpz_int m);
template cpp_int fastModularExp(cpp_int a, cpp_int n, cpp_int m);

/* 
template <typename T>
T fastModularExp2(T base, T exponent, T mod){
	
}
template cpp_int fastModularExp2(cpp_int a, cpp_int n, cpp_int m);
*/

template <typename T>
T naivePower(T base, T exponent){
	T res = base;
	//cout << "base: " << base << " exponent: " << exponent << endl;
	for (T i = 0; i + 1 < exponent; i++){
		//cout << "res: " << res << " base: " << base << "\t i: " << i << endl;
		res = res * base;
	}
	return res;
}


// complexity: O(n²)
template <typename T>
bool millerRabinQuadratic(T n){
	assert(n >= 3 && n % 2 == 1);

	for (T i = 0; i < MR_REPETITIONS; i++){
		boost::random::mt19937 gen(clock());
		boost::random::uniform_int_distribution<T> dist(2, n - 2);
		T a = dist(gen);

		T d = n - 1;
		T z;
		for (z = 0; !(d % 2); z++){
			d /= 2;
		}
		//cout << "d * 2 ^ z = " << d << " * 2^" << z << endl;
		T b = naivePower(a, d) % n;
		while (d != n - 1 && b != 1 && b != n - 1){
			b = (b * b) % n;
			d *= 2;
			//cout << "temp2: " << temp << endl;
		}
		if (b != n - 1 && d % 2 == 0){
			return false;
		}
	}
	return true;
}
//template bool millerRabinQuadratic(unsigned long long n);
template bool millerRabinQuadratic(cpp_int n);
template bool millerRabinQuadratic(mpz_int n);


// complexity of the algorithm: O(log n)
template <typename T>
bool millerRabinExpBySquaring(T n){ 
	assert(n >= 3 && n % 2 == 1);
	for (T i = 0; i < MR_REPETITIONS; i++){
		// a is the randomly chosen base
		boost::random::mt19937 gen(clock());
		boost::random::uniform_int_distribution<T> dist(2, n - 2);
		T a = dist(gen);

		if (n == a) continue;  

		T d = n - 1;
		T z;
		for (z = 0; !(d % 2); z++){
			d /= 2;
		}

		T b = expBySquaring<T>(a, d) % n;
		//cout << b << " = " << a << "^" << d << " mod " << n << endl;

		while (d != n - 1 && b != 1 && b != n - 1){
			b = (b * b) % n;
			d *= 2;
		}
		if (b != n - 1 && d % 2 == 0){
			return false;
		}
	}
	return true;
}
//template bool millerRabinExpBySquaring(unsigned long long n);
template bool millerRabinExpBySquaring(cpp_int n);
template bool millerRabinExpBySquaring(mpz_int n);


// complexity of the algorithm: O(log n)
template <typename T>
bool millerRabinFastModExp(T n){

	T s = n - 1;
	while (s % 2 == 0){
		s /= 2;
	}

	for (int i = 0; i < MR_REPETITIONS; i++){

		// check if a can be duplicated
		boost::random::mt19937 gen(clock());
		boost::random::uniform_int_distribution<T> dist(2, n - 2);
		T a = dist(gen);

		T temp = s;
		//cout << "temp: " << temp << endl;
		T x = fastModularExp(a, temp, n);
		
		while (temp <= n - 1 && x != 1 && x != n - 1){
			x = (x * x) % n;
			temp *= 2;
			//cout << "temp2: " << temp << endl;
		}
		if (x != n - 1 && temp % 2 == 0){
			return false;
		}
	}

	return true;
}
template bool millerRabinFastModExp(cpp_int n);
template bool millerRabinFastModExp(mpz_int n);

// complexity of the algorithm: O(log n)
template <typename T>
bool millerRabinBitShift(T n){
	for (T i = 0; i < MR_REPETITIONS; i++){

		boost::random::mt19937 gen(clock());
		boost::random::uniform_int_distribution<T> dist(2, n - 2);
		T a = dist(gen);

	  T s, d, b, e, x;
	 
	  // Factor n-1 as d 2^s
	  for(s = 0, d = n - 1; !(d & 1); s++) // d & 1 is nothing else than d % 2 as bitwise operation
	  d >>= 1;													// d >>= x is nothing but the integer division by 2^x
	  // in every step s is increased, d is basically (integer-) divided by 2
	 
	  // x = a^d mod n using exponentiation by squaring
	  // The squaring overflows for 64Bit n >= 2^32
	  for(x = 1, b = a % n, e = d; e; e >>= 1) {
	    if(e & 1)
	      x = (x * b) % n;
	    b = (b * b) % n;
	  }
	 
	  // Verify a^(d 2^[0…s-1]) mod n != 1
	  // if one of the statements is true n may be a prime number
	  
	  T z;

	  while(s-- > 0) {
	    z = x;
	    x = (x * x) % n;

	    // if (x * x) % n = 1 then there is a non-trivial square root of 1 mod n 
	    //		i.e. n is definitely composite
	    // also if z is either one or n - 1 there has to be a non-trivial square root of 1 mod n
	    if(x == 1 && z != 1 && z != n - 1)
	      return false;
	  }
	  if (x != 1)
	  	return false;
	}
	return true;
}
//template bool millerRabinBitShift(unsigned long long n);
template bool millerRabinBitShift(cpp_int n);
template bool millerRabinBitShift(mpz_int n);


// complexity of the algorithm: O(log n)
template <typename T>
bool millerRabinBitShift2(T n){
	T s, d;
  // Factor n-1 as d 2^s
  for(s = 0, d = n - 1; !(d & 1); s++) // d & 1 is nothing else than d % 2 as bitwise operation
  d >>= 1;													// d >>= x is nothing but the integer division by 2^x
  // in every step s is increased, d is basically (integer-) divided by 2

	for (T i = 0; i < MR_REPETITIONS; i++){

		boost::random::mt19937 gen(clock());
		boost::random::uniform_int_distribution<T> dist(2, n - 2);
		T a = dist(gen);

	  T b, e, x;
	 
	  // x = a^d mod n using exponentiation by squaring
	  // The squaring overflows for 64Bit n >= 2^32
	  for(x = 1, b = a % n, e = d; e; e >>= 1) {
	    if(e & 1)
	      x = (x * b) % n;
	    b = (b * b) % n;
	  }
	 
	  // Verify a^(d 2^[0…s-1]) mod n != 1
	  // if one of the statements is true n may be a prime number
	  
	  T z, s2 = s;

	  while(s2-- > 0) {
	    z = x;
	    x = (x * x) % n;

	    // if (x * x) % n = 1 then there is a non-trivial square root of 1 mod n 
	    //		i.e. n is definitely composite
	    // also if z is either one or n - 1 there has to be a non-trivial square root of 1 mod n
	    if(x == 1 && z != 1 && z != n - 1)
	      return false;
	  }
	  if (x != 1)
	  	return false;
	}
	return true;
}
//template bool millerRabinBitShift2(unsigned long long n);
template bool millerRabinBitShift2(cpp_int n);
template bool millerRabinBitShift2(mpz_int n);