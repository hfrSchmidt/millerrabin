set terminal png size 1920, 1080
set output "millerRabin_cycles.png"
set xlabel "input size [only prime numbers]"
set ylabel "cycles"
set yrange [0:10000000]
set key spacing 1.5
set key outside
set key font ",14"
plot 'cycleMeasurements_mr1.dat' u 1:4 title 'MRQuadratic' w l, \
'cycleMeasurements_mr2.dat' u 1:4 title 'MR2' w l, \
'cycleMeasurements_mr3.dat' u 1:4 title 'MR3' w l, \
'cycleMeasurements_mr4.dat' u 1:4 title 'MRBitShift' w l, \
'cycleMeasurements_mr5.dat' u 1:4 title 'MRBitShift2' w l, \
'cycleMeasurements_mr5mpz.dat' u 1:4 title 'MRBitShift2 [gmp_int]' w l, \
'cycleMeasurements_BoostMR.dat' u 1:4 title 'Boost MR' w l
