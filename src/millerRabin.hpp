#ifndef MILLERRABIN_HPP
#define MILLERRABIN_HPP

#include <cassert>
#include <cstdio>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <boost/multiprecision/cpp_int.hpp>
//#include <gmpxx.h>
#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/random.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/multiprecision/miller_rabin.hpp>

/* 
defines the number of repetitions of the miller-rabin test
The probability of obtaining a false positive is 4^-l [1],
with l = No. of repetitions.  
Therefore 20 repetitions yield an error probability of less than 9.09*10^-13,
which should be enough in this case of application.

[1] Dietzfelbinger, M. (2004) "Primality Testing in Polynomial Time - From 
randomized algorithms to PRIMES is in P". Lecture Notes in Computer Science:3000.
Berlin: Springer-Verlag
*/ 
#define MR_REPETITIONS 20

template <typename T>
extern T expBySquaring(T base, T exponent);

template <typename T>
extern T fastModularExp(T a, T n, T m);

template <typename T>
extern bool millerRabinQuadratic(T n);

template <typename T>
extern bool millerRabinExpBySquaring(T n);

template <typename T>
extern bool millerRabinFastModExp(T n);

template <typename T>
extern bool millerRabinBitShift(T n);

template <typename T>
extern bool millerRabinBitShift2(T n);

#endif