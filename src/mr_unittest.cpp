#include <gtest/gtest.h>
#include "millerRabin.hpp"

int main(int argc, char **argv){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

/*
TEST functions for millerRabin.cpp for correctness
*/
//mpz_int n2 = 74838457648748954900050464578792347604359487509026452654305481;

TEST(expBySq, selfDefinedInput){
	ASSERT_TRUE(expBySquaring<boost::multiprecision::cpp_int>(2, 8) == 256);
	ASSERT_TRUE(expBySquaring<boost::multiprecision::cpp_int>(0, 1) == 0);
	ASSERT_TRUE(expBySquaring<boost::multiprecision::cpp_int>(2, 0) == 1);
	ASSERT_TRUE(expBySquaring<boost::multiprecision::cpp_int>(4, 2) == 16);
}

TEST(fastModExp, selfDefinedInput){
	ASSERT_TRUE(fastModularExp<boost::multiprecision::cpp_int>(2, 12, 3) == 1);
	ASSERT_TRUE(fastModularExp<boost::multiprecision::cpp_int>(13, 2345, 5) == 3);
	ASSERT_TRUE(fastModularExp<boost::multiprecision::cpp_int>(1729, 1727, 3) == 1);
	ASSERT_TRUE(fastModularExp<boost::multiprecision::cpp_int>(4, 2, 2) == 0);	
}


TEST(millerRabinQuadratic, simplePrimeNumbers){
	boost::multiprecision::cpp_int n = 13;

	ASSERT_TRUE(millerRabinQuadratic<boost::multiprecision::cpp_int>(n));

	n = 17;

	ASSERT_TRUE(millerRabinQuadratic<boost::multiprecision::cpp_int>(n));

	n = 23;

	ASSERT_TRUE(millerRabinQuadratic<boost::multiprecision::cpp_int>(n));

	n = 41;

	ASSERT_TRUE(millerRabinQuadratic<boost::multiprecision::cpp_int>(n));
}


TEST(millerRabinExpBySquaring, PrimeNumbers){
	boost::multiprecision::cpp_int n = 13;

	ASSERT_TRUE(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n));

	n = 17;

	ASSERT_TRUE(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n));

	n = 2879;

	ASSERT_TRUE(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n));

	n = 3041;

	ASSERT_TRUE(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n));

	n = 21143;

	ASSERT_TRUE(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n));
}



TEST(millerRabinExpBySquaring, CarmichaelNos){
	boost::multiprecision::cpp_int n = 1729;	

	ASSERT_TRUE(!(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n)));

	n = 561;

	ASSERT_TRUE(!(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n)));

	n = 1105;

	ASSERT_TRUE(!(millerRabinExpBySquaring<boost::multiprecision::cpp_int>(n)));	
}



TEST(millerRabinFastModExp, largeNumbers){
	boost::multiprecision::cpp_int n = 561;

	ASSERT_TRUE(!(millerRabinFastModExp<boost::multiprecision::cpp_int>(n)));

	n = 859;

	ASSERT_TRUE(millerRabinFastModExp<boost::multiprecision::cpp_int>(n));

	n = 13;

	ASSERT_TRUE(millerRabinFastModExp<boost::multiprecision::cpp_int>(n));		

	n = 999331;

	ASSERT_TRUE(millerRabinFastModExp<boost::multiprecision::cpp_int>(n));		

	n = 32416175167;

	ASSERT_TRUE(millerRabinFastModExp<boost::multiprecision::cpp_int>(n));	
}

TEST(millerRabinFastModExp, CarmichaelNos){
	boost::multiprecision::cpp_int n = 1729;	

	ASSERT_TRUE(!(millerRabinFastModExp<boost::multiprecision::cpp_int>(n)));

	n = 561;

	ASSERT_TRUE(!(millerRabinFastModExp<boost::multiprecision::cpp_int>(n)));

	n = 1105;

	ASSERT_TRUE(!(millerRabinFastModExp<boost::multiprecision::cpp_int>(n)));	
}

TEST(millerRabinBitShift, asd){
	boost::multiprecision::cpp_int n = 13;	

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::cpp_int>(n));	

	n = 561;

	ASSERT_TRUE(!(millerRabinBitShift<boost::multiprecision::cpp_int>(n)));

	n = 1105;

	ASSERT_TRUE(!(millerRabinBitShift<boost::multiprecision::cpp_int>(n)));

	n = 999331;

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::cpp_int>(n));		

	n = 32416175167;

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::cpp_int>(n));	

	n = 32416190071;

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::cpp_int>(n));

	n = 25326001;

	ASSERT_TRUE(!(millerRabinBitShift<boost::multiprecision::cpp_int>(n)));

	boost::multiprecision::cpp_int p("74838457648748954900050464578792347604359487509026452654305481");

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::cpp_int>(p));	
}

TEST(millerRabinBitShift, MPZ_int){
	boost::multiprecision::mpz_int n = 13;	

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::mpz_int>(n));	

	n = 561;

	ASSERT_TRUE(!(millerRabinBitShift<boost::multiprecision::mpz_int>(n)));

	n = 1105;

	ASSERT_TRUE(!(millerRabinBitShift<boost::multiprecision::mpz_int>(n)));

	n = 999331;

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::mpz_int>(n));		

	n = 32416175167;

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::mpz_int>(n));	

	n = 32416190071;

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::mpz_int>(n));

	n = 25326001;

	ASSERT_TRUE(!(millerRabinBitShift<boost::multiprecision::mpz_int>(n)));

	boost::multiprecision::mpz_int p("74838457648748954900050464578792347604359487509026452654305481");

	ASSERT_TRUE(millerRabinBitShift<boost::multiprecision::mpz_int>(p));	
}

TEST(millerRabinBitShift2, CPP_int){
	boost::multiprecision::cpp_int n = 13;	

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::cpp_int>(n));	

	n = 561;

	ASSERT_TRUE(!(millerRabinBitShift2<boost::multiprecision::cpp_int>(n)));

	n = 1105;

	ASSERT_TRUE(!(millerRabinBitShift2<boost::multiprecision::cpp_int>(n)));

	n = 999331;

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::cpp_int>(n));		

	n = 32416175167;

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::cpp_int>(n));	

	n = 32416190071;

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::cpp_int>(n));

	n = 25326001;

	ASSERT_TRUE(!(millerRabinBitShift2<boost::multiprecision::cpp_int>(n)));

	boost::multiprecision::cpp_int p("74838457648748954900050464578792347604359487509026452654305481");

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::cpp_int>(p));	
}

TEST(millerRabinBitShift2, MPZ_int){
	boost::multiprecision::mpz_int n = 13;	

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::mpz_int>(n));	

	n = 561;

	ASSERT_TRUE(!(millerRabinBitShift2<boost::multiprecision::mpz_int>(n)));

	n = 1105;

	ASSERT_TRUE(!(millerRabinBitShift2<boost::multiprecision::mpz_int>(n)));

	n = 999331;

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::mpz_int>(n));		

	n = 32416175167;

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::mpz_int>(n));	

	n = 32416190071;

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::mpz_int>(n));

	n = 25326001;

	ASSERT_TRUE(!(millerRabinBitShift2<boost::multiprecision::mpz_int>(n)));

	boost::multiprecision::mpz_int p("74838457648748954900050464578792347604359487509026452654305481");

	ASSERT_TRUE(millerRabinBitShift2<boost::multiprecision::mpz_int>(p));	
}