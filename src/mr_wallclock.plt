set terminal png size 1920, 1080
set output "millerRabin_wallclock.png"
set xlabel "input size [only prime numbers]"
set ylabel "ms"
set yrange [0:6]
set key spacing 1.5
set key outside
set key font ",14"
plot 'clockMeasurements_mr1.dat' u 1:($4/1000) title 'MRQuadratic' w l, \
'clockMeasurements_mr2.dat' u 1:($4/1000) title 'MR2' w l, \
'clockMeasurements_mr3.dat' u 1:($4/1000) title 'MR3' w l, \
'clockMeasurements_mr4.dat' u 1:($4/1000) title 'MRBitShift' w l, \
'clockMeasurements_mr5.dat' u 1:($4/1000) title 'MRBitShift2' w l, \
'clockMeasurements_mr5mpz.dat' u 1:($4/1000) title 'MRBitShift2[gmp_int]' w l, \
'clockMeasurements_BoostMR.dat' u 1:($4/1000) title 'Boost MR' w l
