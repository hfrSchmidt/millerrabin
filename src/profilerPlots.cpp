#include "profilerPlots.hpp"

bool fileExists(std::string fileName){
  std::ifstream ifile(fileName.c_str());
  return ifile;
}

void makePlots(std::vector<std::string> filenamesClock, std::vector<std::string> filenamesCycle, int whatToPlot){

  for (size_t i = 0; i < filenamesClock.size(); i++){
    if(!fileExists(filenamesClock[i])){
      printf("%s%s%s\n", "The file ", filenamesClock[i].c_str(), " does not exist.");
      exit(EXIT_FAILURE);
    }
  }

  for (size_t i = 0; i < filenamesCycle.size(); i++){
    if(!fileExists(filenamesCycle[i])){
      printf("%s%s%s\n", "The file ", filenamesCycle[i].c_str(), " does not exist.");
      exit(EXIT_FAILURE);
    }
  }
  
  if (whatToPlot == 0){
    if (system(NULL)) system("gnuplot ./src/mr_wallclock.plt");
    else exit(EXIT_FAILURE);

    if (system(NULL)) system("gnuplot ./src/mr_cycles.plt");
    else exit(EXIT_FAILURE);
  }
  
}