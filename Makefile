SHELL = /bin/sh

.SUFFIXES:
.SUFFIXES: .cpp .o

rwildcard=$(foreach d, $(wildcard $1*), $(call rwildcard, $d/, $2) $(filter $(subst *, %, $2), $d))
OBJECTS = $(call rwildcard, ./, *.o)

# Google Test:
GTEST_INCLUDE_DIR = /usr/include/gtest
GTEST_LINK_FLAGS = -L/usr/lib/ -pthread
# the libgtest.a library needs to be linked AFTER the respective object files
GTEST_LIBRARY = -lgtest

CXX := g++
RM := rm -f
DEBUGGINGFLAGS = -g

TESTINGFLAGS = -DENABLE_TESTS
MEASURINGFLAGS = -DMEASURE
PLOTFLAGS = -DPLOTS

CPPFLAGS = -isystem $(GTEST_INCLUDE_DIR)
CXXFLAGS = -Wall -Wextra
GMPFLAGS = -lgmpxx -lgmp


################ MillerRabin main file
MR_SOURCES = ./src/millerRabin.cpp
MR_OBJS = millerRabin.o
MR_EXECUTABLE = $(MR_OBJS:.o=)


################ test files + recompilation of millerRabin main file
#### recompilation necessary because of changing DEFINES 

MR_TEST_SOURCES = ./src/mr_unittest.cpp
MR_TEST_OBJS = mr_unittest.o
MR_TEST_MAIN_OBJS = t_mr.o
MR_TEST_EXECUTABLE = $(MR_TEST_OBJS:.o=)


################ profiler files + recompilation of millerRabin main file
#### recompilation necessary because of changing DEFINES 

PROFILER_SOURCES = ./src/profiler.cpp
MR_PROFILER_OBJS = mrProfiler.o

MR_MEASURE_OBJS = m_millerRabin.o
MR_MEASURE_EXECUTABLE = $(MR_MEASURE_OBJS:.o=)


################ plot files + recompilation of millerRabin main file
#### recompilation necessary because of changing DEFINES 

PLOT_SOURCES = ./src/profilerPlots.cpp
MR_PLOT_OBJS = mrProfilerPlots.o

MR_PROFILER_PLOT_OBJS = p_profiler.o 
RE_MR_PLOT_OBJS = p_millerRabin.o
MR_PLOT_EXECUTABLE = $(RE_MR_PLOT_OBJS:.o=)


.PHONY: all
all: build

.PHONY: build
build: $(MR_EXECUTABLE)

$(MR_EXECUTABLE) : $(MR_OBJS)
	$(CXX) $< -o $@

$(MR_OBJS) : $(MR_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@
	
.PHONY: test
test: $(MR_TEST_EXECUTABLE)

$(MR_TEST_EXECUTABLE) : $(MR_TEST_OBJS) $(MR_TEST_MAIN_OBJS)
	$(CXX) $(GTEST_LINK_FLAGS) $^ $(GTEST_LIBRARY) $(GMPFLAGS) -o $@
	./$(MR_TEST_EXECUTABLE)

$(MR_TEST_OBJS) : $(MR_TEST_SOURCES) 
	$(CXX) $(CPPFLAGS) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(MR_TEST_MAIN_OBJS) : $(MR_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(TESTINGFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: measure
measure: $(MR_MEASURE_EXECUTABLE)

$(MR_MEASURE_EXECUTABLE) : $(MR_MEASURE_OBJS) $(MR_PROFILER_OBJS)
	$(CXX) $(DEBUGGINGFLAGS) $^ $(GMPFLAGS) -o $@
	./removeDat.sh
	./$(MR_MEASURE_EXECUTABLE)

$(MR_MEASURE_OBJS) : $(MR_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(MEASURINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(MR_PROFILER_OBJS) : $(PROFILER_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: plots
plots: $(MR_PLOT_EXECUTABLE)

$(MR_PLOT_EXECUTABLE) : $(MR_PROFILER_PLOT_OBJS) $(MR_PLOT_OBJS) $(RE_MR_PLOT_OBJS) 
	$(CXX) $^ $(GMPFLAGS) -o $@
	./$(MR_PLOT_EXECUTABLE)

$(RE_MR_PLOT_OBJS) : $(MR_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(PLOTFLAGS) $(CXXFLAGS) -c $< -o $@

$(MR_PLOT_OBJS) : $(PLOT_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(MR_PROFILER_PLOT_OBJS) : $(PROFILER_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@	

.PHONY: clean
clean:
	$(RM) $(OBJECTS) $(MR_EXECUTABLE) $(MR_TEST_EXECUTABLE) $(MR_MEASURE_EXECUTABLE) $(MR_PLOT_EXECUTABLE)
